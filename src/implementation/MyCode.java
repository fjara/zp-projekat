package implementation;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateEncodingException;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.Vector;

import code.GuiException;
import gui.Constants;
import x509.v3.CodeV3;

import org.bouncycastle.asn1.ASN1Integer;
import org.bouncycastle.asn1.ASN1Object;
import org.bouncycastle.asn1.ASN1Primitive;
import org.bouncycastle.asn1.DERGeneralString;
import org.bouncycastle.asn1.DERSet;
import org.bouncycastle.asn1.x500.X500Name;
import org.bouncycastle.asn1.x500.X500NameBuilder;
import org.bouncycastle.asn1.x500.style.BCStyle;
import org.bouncycastle.asn1.x509.AlgorithmIdentifier;
import org.bouncycastle.asn1.x509.Attribute;
import org.bouncycastle.asn1.x509.Extension;
import org.bouncycastle.asn1.x509.KeyUsage;
import org.bouncycastle.asn1.x509.SubjectDirectoryAttributes;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.cert.CertIOException;
import org.bouncycastle.cert.X509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509CertificateConverter;
import org.bouncycastle.cert.jcajce.JcaX509CertificateHolder;
import org.bouncycastle.cert.jcajce.JcaX509ExtensionUtils;
import org.bouncycastle.cert.jcajce.JcaX509v3CertificateBuilder;
import org.bouncycastle.cms.CMSException;
import org.bouncycastle.cms.CMSProcessableByteArray;
import org.bouncycastle.cms.CMSSignedData;
import org.bouncycastle.cms.CMSSignedDataGenerator;
import org.bouncycastle.cms.CMSTypedData;
import org.bouncycastle.crypto.params.AsymmetricKeyParameter;
import org.bouncycastle.crypto.util.PrivateKeyFactory;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.jcajce.JcaPEMWriter;
import org.bouncycastle.operator.ContentSigner;
import org.bouncycastle.operator.ContentVerifierProvider;
import org.bouncycastle.operator.DefaultDigestAlgorithmIdentifierFinder;
import org.bouncycastle.operator.DefaultSignatureAlgorithmIdentifierFinder;
import org.bouncycastle.operator.OperatorCreationException;
import org.bouncycastle.operator.bc.BcRSAContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentSignerBuilder;
import org.bouncycastle.operator.jcajce.JcaContentVerifierProviderBuilder;
import org.bouncycastle.pkcs.PKCS10CertificationRequest;
import org.bouncycastle.pkcs.PKCS10CertificationRequestBuilder;
import org.bouncycastle.pkcs.PKCSException;
import org.bouncycastle.pkcs.jcajce.JcaPKCS10CertificationRequest;
import org.bouncycastle.util.CollectionStore;
import org.bouncycastle.util.Store;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.bouncycastle.x509.extension.X509ExtensionUtil;

import java.security.interfaces.RSAPublicKey;

public class MyCode extends CodeV3 {

	private static String fileName = "myKeyStore.p12";
	private static char[] password = "root".toCharArray();
	private KeyStore myKeyStore;
	private PKCS10CertificationRequest req;

	public MyCode(boolean[] algorithm_conf, boolean[] extensions_conf, boolean extensions_rules) throws GuiException {
		super(algorithm_conf, extensions_conf, extensions_rules);

	}

	@Override
	public boolean canSign(String arg0) {
		try {
			X509Certificate certificate = (X509Certificate) myKeyStore.getCertificate(arg0);
			if (certificate.getBasicConstraints() != -1) {
				boolean[] keyUsage = certificate.getKeyUsage();
				if (keyUsage != null)
					if (keyUsage[Constants.KEY_CERT_SIGN]) {
						return true;
					}
			}

		} catch (KeyStoreException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}

	@Override
	public boolean exportCSR(String file, String keypair_name, String algorithm) {
		FileOutputStream fos = null;
		try {
			X509Certificate certificate;

			certificate = (X509Certificate) myKeyStore.getCertificate(keypair_name);

			X500NameBuilder builder = new X500NameBuilder();
			String str = certificate.getSubjectDN().toString();
			str = str.replaceAll(" ", "");
			String[] arr = str.split(",");

			for (int i = 0; i < arr.length; i++) {
				String[] arr2 = arr[i].split("=");
				if (arr2.length > 1) {
					if ("CN".equals(arr2[0])) {
						builder.addRDN(BCStyle.CN, arr2[1]);
					} else if ("O".equals(arr2[0])) {
						builder.addRDN(BCStyle.O, arr2[1]);
					} else if ("OU".equals(arr2[0])) {
						builder.addRDN(BCStyle.OU, arr2[1]);
					} else if ("L".equals(arr2[0])) {
						builder.addRDN(BCStyle.L, arr2[1]);
					} else if ("ST".equals(arr2[0])) {
						builder.addRDN(BCStyle.ST, arr2[1]);
					} else if ("C".equals(arr2[0])) {
						builder.addRDN(BCStyle.C, arr2[1]);
					}

				}
			}
			X500Name name = builder.build();

			PKCS10CertificationRequestBuilder reqBuilder = new PKCS10CertificationRequestBuilder(name,
					SubjectPublicKeyInfo.getInstance(certificate.getPublicKey().getEncoded()));

			AlgorithmIdentifier signature = new DefaultSignatureAlgorithmIdentifierFinder().find(algorithm);

			AlgorithmIdentifier digest = new DefaultDigestAlgorithmIdentifierFinder().find(signature);

			AsymmetricKeyParameter parameter = PrivateKeyFactory
					.createKey(myKeyStore.getKey(keypair_name, null).getEncoded());

			PKCS10CertificationRequest request = reqBuilder
					.build(new BcRSAContentSignerBuilder(signature, digest).build(parameter));

			fos = new FileOutputStream(file);
			fos.write(request.getEncoded());
			fos.close();
			return true;
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
		e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		}

		return false;
	}

	@Override
	public boolean exportCertificate(String file, String keypair_name, int encoding, int format) {

		FileOutputStream fos = null;
		OutputStreamWriter osw = null;
		JcaPEMWriter pw = null;

		try {

			fos = new FileOutputStream(file);
			if (encoding == Constants.DER) {
				if (format == Constants.HEAD) {

					fos.write(myKeyStore.getCertificate(keypair_name).getEncoded());
					fos.close();
				}
			} else if (encoding == Constants.PEM) {
				osw = new OutputStreamWriter(fos);
				pw = new JcaPEMWriter(osw);
				if (format == Constants.CHAIN) {
					pw.writeObject(myKeyStore.getCertificate(keypair_name));
					Certificate[] cs = myKeyStore.getCertificateChain(keypair_name);

					if (cs != null)

						for (int i = 0; i < cs.length; i++) {
							pw.writeObject(cs[i]);
						}

				} else {
					pw.writeObject(myKeyStore.getCertificate(keypair_name));
				}
				pw.close();
				osw.close();
			}

		
			return true;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (CertificateEncodingException e) {
			e.printStackTrace();
			try {
				fos.close();
			} catch (IOException e1) {
				e1.printStackTrace();
				return false;
			}
			return false;
		} catch (KeyStoreException e) {
			e.printStackTrace();

			return false;
		}
	}

	@Override
	public boolean exportKeypair(String keypair_name, String file, String pass) {

		try {
			KeyStore forExport = KeyStore.getInstance("PKCS12", new BouncyCastleProvider());
			forExport.load(null, pass.toCharArray());

			Key key = myKeyStore.getKey(keypair_name, null);
			Certificate[] certificates = myKeyStore.getCertificateChain(keypair_name);
			forExport.setKeyEntry(keypair_name, key, pass.toCharArray(), certificates);

			FileOutputStream fos = null;

			fos = new FileOutputStream(file);
			forExport.store(fos, pass.toCharArray());

			return true;
		} catch (KeyStoreException e) {
			return false;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		} catch (CertificateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public String getCertPublicKeyAlgorithm(String arg0) {
		return "RSA";
	}

	@Override
	public String getCertPublicKeyParameter(String arg0) {
		Certificate certificate;
		try {
			certificate = myKeyStore.getCertificate(arg0);
			RSAPublicKey key = (RSAPublicKey) certificate.getPublicKey();
			return key.getModulus().bitLength() + "";
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public String getSubjectInfo(String arg0) {
		X509Certificate certificate = null;
		try {
			certificate = (X509Certificate) myKeyStore.getCertificate(arg0);

			String str = certificate.getSubjectDN().toString();
			String[] arr = str.split(",");
			String newArr = "";
			for (int i = 0; i < arr.length; i++) {
				String[] arr2 = arr[i].split("=");
				if (arr2.length > 1) {
					newArr += arr[i] + ",";

				}
			}
			if (newArr.length() > 0) {
				newArr = newArr.substring(0, newArr.length() - 1);
			}
			return newArr.replaceAll(" ", "");

		} catch (KeyStoreException e) {

			e.printStackTrace();
			return null;
		}

	}

	@Override
	public boolean importCAReply(String file, String keypair_name) {
		FileInputStream fis = null;
		CMSSignedData CMSsignedData = null;
		try {
			fis = new FileInputStream(file);
			CMSsignedData = new CMSSignedData(fis);

			Store<X509CertificateHolder> store = CMSsignedData.getCertificates();
			Collection<X509CertificateHolder> holders = store.getMatches(null);

			Certificate[] certificates = new X509Certificate[holders.size()];
			Iterator<X509CertificateHolder> it = holders.iterator();
			for (int i = 0; i < certificates.length; i++) {
				certificates[i] = new JcaX509CertificateConverter().setProvider(new BouncyCastleProvider())
						.getCertificate(it.next());
			}

			PrivateKey privateKey = (PrivateKey) myKeyStore.getKey(keypair_name, password);
			myKeyStore.setKeyEntry(keypair_name, privateKey, password, certificates);

			FileOutputStream output = new FileOutputStream(fileName);
			myKeyStore.store(output, password);
			output.close();

			fis.close();
			loadKeypair(keypair_name);
			return true;
		} catch (CMSException e) {

		} catch (FileNotFoundException e) {

		} catch (IOException e) {

		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public String importCSR(String arg0) {
		FileInputStream fis = null;
		DataInputStream dis = null;

		try {
			fis = new FileInputStream(arg0);
			dis = new DataInputStream(fis);
			byte csrData[] = new byte[dis.available()];
			dis.readFully(csrData);
			req = new PKCS10CertificationRequest(csrData);
			ContentVerifierProvider p = new JcaContentVerifierProviderBuilder().setProvider(new BouncyCastleProvider())
					.build(req.getSubjectPublicKeyInfo());

			fis.close();
			dis.close();
			if (req.isSignatureValid(p)) {
				return req.getSubject().toString();

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (PKCSException e) {
			e.printStackTrace();
		}
		return null;

	}

	@Override
	public boolean importCertificate(String file, String keypair_name) {

		FileInputStream fis = null;
		try {
			fis = new FileInputStream(file);

			 CertificateFactory factory = CertificateFactory.getInstance("X.509");
			X509Certificate c = (X509Certificate) factory.generateCertificate(fis);

			myKeyStore.setCertificateEntry(keypair_name, c);

			FileOutputStream output = new FileOutputStream(fileName);
			myKeyStore.store(output, password);
			output.close();
			fis.close();

			return true;
		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
			e.printStackTrace();
		} catch (CertificateException e) {
		
			e.printStackTrace();
		} catch (KeyStoreException e) {
			
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
		
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean importKeypair(String keypair_name, String file, String pass) {

		try {
			KeyStore forImport = KeyStore.getInstance("PKCS12", new BouncyCastleProvider());
			FileInputStream fis = new FileInputStream(file);
			forImport.load(fis, pass.toCharArray());

			Enumeration<String> list = forImport.aliases();
			while (list.hasMoreElements()) {
				String str = list.nextElement();
				Key key = forImport.getKey(str, pass.toCharArray());
				Certificate[] certificates = forImport.getCertificateChain(str);
				myKeyStore.setKeyEntry(keypair_name, key, password, certificates);
				FileOutputStream output = new FileOutputStream(fileName);
				myKeyStore.store(output, password);
			}
			return true;
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return false;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		} catch (CertificateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
			return false;
		}

	}

	
	@Override
	public int loadKeypair(String arg0) {

		try {

			X509Certificate certificate = (X509Certificate) myKeyStore.getCertificate(arg0);
			/* Osnovni info */
			access.setSerialNumber(certificate.getSerialNumber().toString());
			access.setVersion(Constants.V3);
			access.setNotAfter(certificate.getNotAfter());
			access.setNotBefore(certificate.getNotBefore());
			access.setPublicKeyAlgorithm(certificate.getPublicKey().getAlgorithm());
			access.setPublicKeyParameter(((RSAPublicKey) certificate.getPublicKey()).getModulus().bitLength() + "");
			access.setPublicKeyDigestAlgorithm(certificate.getSigAlgName());
			access.setIssuerSignatureAlgorithm(certificate.getPublicKey().getAlgorithm());
			access.setSubjectSignatureAlgorithm(certificate.getPublicKey().getAlgorithm());

			String str = certificate.getIssuerDN().toString();
			String str2 = certificate.getSubjectDN().toString();
			// izbacuju se ona polja koja nemaju vrednost da setIssuer ne bi
			// pucao
			String[] arr = str.split(",");
			String[] arrSubject = str2.split(",");
			String newArr = "";
			String newArr2 = "";
			for (int i = 0; i < arr.length; i++) {
				String[] arr2 = arr[i].split("=");
				if (arr2.length > 1) {
					newArr += arr[i] + ",";

				}
			}

			for (int i = 0; i < arrSubject.length; i++) {
				String[] arr2 = arrSubject[i].split("=");
				if (arr2.length > 1) {
					newArr2 += arrSubject[i] + ",";

				}
			}

			if (newArr.length() > 0) {
				newArr = newArr.substring(0, newArr.length() - 1);
			}
			if (newArr2.length() > 0) {
				newArr2 = newArr2.substring(0, newArr2.length() - 1);
			}
			newArr = newArr.replaceAll(" ", "");
			newArr2 = newArr2.replaceAll(" ", "");
			access.setIssuer(newArr);
			access.setSubject(newArr2);
			access.setIssuerSignatureAlgorithm(certificate.getPublicKey().getAlgorithm());
			/*
			 * str = certificate.getSubjectDN().toString(); String [] step1= str.split(",");
			 * HashMap<String,String> hashMap=new HashMap<String,String>(); for (int i = 0;
			 * i < step1.length; i++) {
			 * 
			 * String [] step2 =step1[i].split("=");
			 * 
			 * if (step2.length>1) { hashMap.put(step2[0], step2[1]);
			 * 
			 * } } if (hashMap.containsKey("CN")) {
			 * access.setSubjectCommonName(hashMap.get("CN")); } if
			 * (hashMap.containsKey("C")) { access.setSubjectCountry(hashMap.get("C")); }if
			 * (hashMap.containsKey("ST")) { access.setSubjectState(hashMap.get("ST")); }if
			 * (hashMap.containsKey("L")) { access.setSubjectLocality(hashMap.get("L")); }if
			 * (hashMap.containsKey("O")) { access.setSubjectOrganization(hashMap.get("O"));
			 * }if (hashMap.containsKey("OU")) {
			 * access.setSubjectOrganizationUnit(hashMap.get("OU")); }
			 * 
			 * // puca za sad ako se ostavi neko polje prazno
			 */
			/* Is Critical */
			Set<String> criticals = certificate.getCriticalExtensionOIDs();
			if (criticals != null) {
				if (criticals.contains(Extension.keyUsage.toString()))
					access.setCritical(Constants.KU, true);
				if (criticals.contains(Extension.inhibitAnyPolicy.toString()))
					access.setCritical(Constants.IAP, true);
				if (criticals.contains(Extension.subjectDirectoryAttributes.toString()))
					access.setCritical(Constants.SDA, true);
			}
			/* Key usage info */

			if (certificate.getKeyUsage() != null) {

				access.setKeyUsage(certificate.getKeyUsage());
			}

			/* Inhibit any Policy info */
			String index = Extension.inhibitAnyPolicy.toString();
			byte[] inhibitAnyPolicy = certificate.getExtensionValue(index);
			if (inhibitAnyPolicy != null) {
				access.setInhibitAnyPolicy(true);
				access.setSkipCerts(JcaX509ExtensionUtils.parseExtensionValue(inhibitAnyPolicy).toString());
			}

			/* Subject Directory Attributes */
			index = Extension.subjectDirectoryAttributes.toString();
			byte[] subject = certificate.getExtensionValue(index);
			if (subject != null) {
				SubjectDirectoryAttributes subjectDirectoryAttributes = SubjectDirectoryAttributes
						.getInstance(JcaX509ExtensionUtils.parseExtensionValue(subject));
				Vector<Attribute> attrs = subjectDirectoryAttributes.getAttributes();
				Iterator<Attribute> it = attrs.iterator();
				while (it.hasNext()) {
					Attribute cur = it.next();
					String type = cur.getAttrType().toString();
					String temp = cur.getAttrValues().toString();
					String val = temp.substring(1, temp.length() - 1);
					int i = -1;
					if (type.equals(BCStyle.DATE_OF_BIRTH.getId())) {
						access.setDateOfBirth(val);
					} else if (type.equals(BCStyle.PLACE_OF_BIRTH.getId())) {
						i = Constants.POB;
					} else if (type.equals(BCStyle.COUNTRY_OF_CITIZENSHIP.getId())) {
						i = Constants.COC;
					} else if (type.equals(BCStyle.GENDER.getId())) {
						access.setGender(val);
					}
					if (i != -1) {
						access.setSubjectDirectoryAttribute(i, val);
					}

				}

			}
			int k;
			k = 1;

			if (certificate.getBasicConstraints() != -1) {

				return 2;
			}
			if (certificate.getSubjectDN().toString().equals(certificate.getIssuerDN().toString())) {
				if (certificate.getKeyUsage() != null && certificate.getKeyUsage()[Constants.KEY_CERT_SIGN]) {
					return 1;
				} else
					return 0;
			} else {
				return 1;
			}

		} catch (KeyStoreException e) {
			e.printStackTrace();
			return -1;
		} catch (IOException e) {
			e.printStackTrace();

			return -1;
		}
	}

	@Override
	public Enumeration<String> loadLocalKeystore() {
		try {
			myKeyStore = KeyStore.getInstance("pkcs12", new BouncyCastleProvider());
			myKeyStore.load(null, password);
			if (!Files.exists(Paths.get(fileName))) {
				FileOutputStream outputFile = new FileOutputStream(fileName);
				myKeyStore.store(outputFile, password);
			}

		} catch (KeyStoreException e) {
			e.printStackTrace();
		}
		catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		FileInputStream fileInput = null;
		try {

			fileInput = new FileInputStream(fileName);
			myKeyStore.load(fileInput, password);
			return myKeyStore.aliases();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		} catch (CertificateException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return null;
		}

	}

	@Override
	public boolean removeKeypair(String arg0) {
		try {
			myKeyStore = KeyStore.getInstance("pkcs12", new BouncyCastleProvider());
			FileInputStream input = new FileInputStream(fileName);
			myKeyStore.load(input, password);

			myKeyStore.deleteEntry(arg0);

			FileOutputStream output = new FileOutputStream(fileName);
			myKeyStore.store(output, password);
			return true;
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return false;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		} catch (CertificateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public void resetLocalKeystore() {
		try {
			myKeyStore = KeyStore.getInstance("pkcs12", new BouncyCastleProvider());
			myKeyStore.load(null, password);

			FileOutputStream outputFile = new FileOutputStream(fileName);
			myKeyStore.store(outputFile, password);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Override
	public boolean saveKeypair(String keyPairName) {
		FileOutputStream output = null;

		// Generisanje para kljuceva pomocu RSA algoritma
		KeyPair generatedKeyPair = generateKeyPair();
		if (generatedKeyPair == null) {
			return false;
		}
		/* kreiranje sertifikata */
		/*---- osnovni deo ----*/

		JcaX509v3CertificateBuilder builder = createBuilderForCertificate(generatedKeyPair);

		/*---- ekstenzije----*/

		// key usage
		boolean ret = addKeyUsageExtension(builder);
		if (ret == false) {
			return false;
		}

		// inhibit any policy
		ret = addInhibitAnyPolicyExtension(builder);
		if (ret == false) {
			return false;
		}

		// subject directory attributes
		ret = addSubjectDirectoryAttributesExtension(builder);
		if (ret == false) {
			return false;
		}

		/* potpisivanje sertifikata */
		ret = signCertificate(keyPairName, generatedKeyPair, builder);
		if (ret == false) {
			return false;
		}
		/* dodavanje sertifikata u local key store */
		ret = addCertificateToLocalKeyStore();
		if (ret == false) {
			return false;
		}
		return true;
	}

	@Override
	public boolean signCSR(String file, String keypair_name, String algorithm) {
		FileOutputStream fos = null;
		try {

			X509Certificate certificate = (X509Certificate) myKeyStore.getCertificate(keypair_name);
			// ovde mozda treba da se parsira
			X500Name name = new JcaX509CertificateHolder(certificate).getSubject();

			PublicKey publicKey = new JcaPKCS10CertificationRequest(this.req).setProvider(new BouncyCastleProvider())
					.getPublicKey();

			JcaX509v3CertificateBuilder builder = new JcaX509v3CertificateBuilder(name,
					new BigInteger(access.getSerialNumber()), access.getNotBefore(), access.getNotAfter(),
					req.getSubject(), publicKey);

			this.addKeyUsageExtension(builder);
			this.addInhibitAnyPolicyExtension(builder);
			this.addSubjectDirectoryAttributesExtension(builder);

			PrivateKey privateKey = (PrivateKey) myKeyStore.getKey(keypair_name, null);
			ContentSigner signer = new JcaContentSignerBuilder(algorithm).setProvider(new BouncyCastleProvider())
					.build(privateKey);
			X509Certificate certificateSigned = new JcaX509CertificateConverter().getCertificate(builder.build(signer));

			ArrayList<JcaX509CertificateHolder> certificates = new ArrayList<>();

			certificates.add(new JcaX509CertificateHolder(certificateSigned));

			Certificate[] chain = myKeyStore.getCertificateChain(keypair_name);
			for (int i = 0; i < chain.length; i++) {
				certificates.add(new JcaX509CertificateHolder((X509Certificate) chain[i]));
			}

			CMSSignedDataGenerator generator = new CMSSignedDataGenerator();

			generator.addCertificates(new CollectionStore<>(certificates));
			CMSTypedData CSMtypedData = new CMSProcessableByteArray(certificateSigned.getEncoded());

			CMSSignedData CSMsignedData = generator.generate(CSMtypedData);
			fos = new FileOutputStream(file);
			fos.write(CSMsignedData.getEncoded());
			fos.close();

			return true;
		} catch (CertificateEncodingException e) {
			e.printStackTrace();
		} catch (KeyStoreException e) {
		e.printStackTrace();
		} catch (InvalidKeyException e) {
		e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnrecoverableKeyException e) {
			e.printStackTrace();
		} catch (OperatorCreationException e) {
			e.printStackTrace();
		} catch (CertificateException e) {
			e.printStackTrace();
		} catch (CMSException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

	private boolean addCertificateToLocalKeyStore() {
		FileOutputStream output = null;
		try {
			output = new FileOutputStream(fileName);
			myKeyStore.store(output, password);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return false;
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return false;
		} catch (CertificateException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			if (output != null)
				try {
					output.close();

				} catch (IOException e) {
					e.printStackTrace();
					return false;
				}
		}
		return true;

	}

	private boolean signCertificate(String keyPairName, KeyPair generatedKeyPair, JcaX509v3CertificateBuilder builder) {
		try {
			String pkda = access.getPublicKeyDigestAlgorithm();
			ContentSigner signer = new JcaContentSignerBuilder(pkda).build(generatedKeyPair.getPrivate());
			X509CertificateHolder certHolder = builder.build(signer);

			X509Certificate[] arr = new X509Certificate[1];
			arr[0] = new JcaX509CertificateConverter().getCertificate(certHolder);

			myKeyStore.setKeyEntry(keyPairName, generatedKeyPair.getPrivate(), null, arr);
			System.out.println("potpisooo sam !");
			return true;
		} catch (OperatorCreationException e) {
			e.printStackTrace();
			return false;
		} catch (CertificateException e) {
			e.printStackTrace();
			return false;
		} catch (KeyStoreException e) {
			e.printStackTrace();
			return false;

		}
	}

	private boolean addSubjectDirectoryAttributesExtension(JcaX509v3CertificateBuilder builder) {

		Vector<Attribute> attributes = new Vector<>();
		String dateOfBirth = access.getDateOfBirth();
		String placeOfBirth = access.getSubjectDirectoryAttribute(Constants.POB);
		String countryOfCitizenship = access.getSubjectDirectoryAttribute(Constants.COC);
		String gender = access.getGender();

		if (!dateOfBirth.isEmpty()) {
			attributes.add(new Attribute(BCStyle.DATE_OF_BIRTH, new DERSet(new DERGeneralString(dateOfBirth))));
		}
		if (!placeOfBirth.isEmpty()) {
			attributes
					.addElement(new Attribute(BCStyle.PLACE_OF_BIRTH, new DERSet(new DERGeneralString(placeOfBirth))));
		}
		if (!countryOfCitizenship.isEmpty()) {
			attributes.addElement(new Attribute(BCStyle.COUNTRY_OF_CITIZENSHIP,
					new DERSet(new DERGeneralString(countryOfCitizenship))));

		}

		if (!gender.isEmpty()) {
			attributes.addElement(new Attribute(BCStyle.GENDER, new DERSet(new DERGeneralString(gender))));

		}
		try {
			builder.addExtension(Extension.subjectDirectoryAttributes, access.isCritical(Constants.SDA),
					new SubjectDirectoryAttributes(attributes));
		} catch (CertIOException e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	private boolean addInhibitAnyPolicyExtension(JcaX509v3CertificateBuilder builder) {
		if (access.getInhibitAnyPolicy() && !access.getSkipCerts().isEmpty()) {
			try {
				builder.addExtension(Extension.inhibitAnyPolicy, access.isCritical(Constants.IAP),
						new ASN1Integer(new BigInteger(access.getSkipCerts())));
			} catch (CertIOException e) {
				e.printStackTrace();
				return false;
			}

		}
		return true;
	}

	private boolean addKeyUsageExtension(JcaX509v3CertificateBuilder builder) {
		int keyUsageInt = 0;
		boolean keyUsageBoolean[] = access.getKeyUsage();
		boolean usageExists = false;
		if (keyUsageBoolean[0]) {
			keyUsageInt = keyUsageInt | KeyUsage.digitalSignature;
			usageExists = true;
		}
		if (keyUsageBoolean[1]) {
			keyUsageInt = keyUsageInt | KeyUsage.nonRepudiation;
			usageExists = true;
		}
		if (keyUsageBoolean[2]) {
			keyUsageInt = keyUsageInt | KeyUsage.keyEncipherment;
			usageExists = true;
		}
		if (keyUsageBoolean[3]) {
			keyUsageInt = keyUsageInt | KeyUsage.dataEncipherment;
			usageExists = true;
		}
		if (keyUsageBoolean[4]) {
			keyUsageInt = keyUsageInt | KeyUsage.keyAgreement;
			usageExists = true;
		}
		if (keyUsageBoolean[5]) {
			keyUsageInt = keyUsageInt | KeyUsage.keyCertSign;
			usageExists = true;
		}
		if (keyUsageBoolean[6]) {
			keyUsageInt = keyUsageInt | KeyUsage.cRLSign;
			usageExists = true;
		}
		if (keyUsageBoolean[7]) {
			keyUsageInt = keyUsageInt | KeyUsage.encipherOnly;
			usageExists = true;
		}
		if (keyUsageBoolean[8]) {
			keyUsageInt = keyUsageInt | KeyUsage.decipherOnly;
			usageExists = true;
		}

		if (usageExists) {
			KeyUsage keyUsage = new KeyUsage(keyUsageInt);
			try {
				builder.addExtension(Extension.keyUsage, access.isCritical(Constants.KU), keyUsage);
			} catch (CertIOException e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;

	}

	private KeyPair generateKeyPair() {
		try {
			/* kreiranje keypair */

			KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA", new BouncyCastleProvider());

			// Koji sha se koristi
			int param1 = Integer.parseInt(access.getPublicKeyParameter());

			// random parametar za rsa alg
			SecureRandom rand = new SecureRandom();

			generator.initialize(param1, rand);

			// Generisanje para kljuceva pomocu RSA algoritma
			return generator.generateKeyPair();
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
			return null;
		}
	}

	private JcaX509v3CertificateBuilder createBuilderForCertificate(KeyPair generatedKeyPair) {
		X500NameBuilder nameBuilder = new X500NameBuilder();
		if (access.getSubjectCommonName() != null) {
			nameBuilder.addRDN(BCStyle.CN, access.getSubjectCommonName());
		}
		if (access.getSubjectOrganization() != null) {
			nameBuilder.addRDN(BCStyle.O, access.getSubjectOrganization());
		}
		if (access.getSubjectOrganizationUnit() != null) {
			nameBuilder.addRDN(BCStyle.OU, access.getSubjectOrganizationUnit());
		}
		if (access.getSubjectLocality() != null) {
			nameBuilder.addRDN(BCStyle.L, access.getSubjectLocality());
		}
		if (access.getSubjectState() != null) {
			nameBuilder.addRDN(BCStyle.ST, access.getSubjectState());
		}
		if (access.getSubjectCountry() != null) {
			nameBuilder.addRDN(BCStyle.C, access.getSubjectCountry());
		}

		// Parametri iz forme levo
		X500Name name = nameBuilder.build();

		// Serijski broj
		BigInteger serialNumber = new BigInteger(access.getSerialNumber());

		// Datum pre
		Date notBefore = access.getNotBefore();

		// Datum posle
		Date notAfter = access.getNotAfter();

		return new JcaX509v3CertificateBuilder(name, serialNumber, notBefore, notAfter, name,
				generatedKeyPair.getPublic());
	}

}
